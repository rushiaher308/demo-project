package com.example.demo;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProductService {
	
	@Autowired
	private ProductRepository productRepository;
	
	@Autowired
	private CategoryRepository categoryRepository;
	
	
	
	public String addProduct(Product product)throws Exception {
		List<Category>listCategory = new ArrayList<>();
		try {
			if(null!=productRepository.findByName(product.getName())) {
				return "product already added with this name";
			}else {
			if(null != product.getCategoryIds()) {
				for(Long id:product.getCategoryIds()) {
					Category c = categoryRepository.findById(id).orElse(null);
					if(null != c) {
						listCategory.add(c);
					}
				}
				product.setCategories(listCategory);
				productRepository.save(product);
			}
			
			return "product saved";
			}
		}catch(Exception e ) {
			throw e;
		}
	}
	
	@SuppressWarnings("unused")
	public List<Product> getProductByCategory(Long Id)throws Exception {
		List<Product>productList = new ArrayList<>();
		try {
		 Category category = categoryRepository.findById(Id).orElse(null);
		if(null != category) {
			productList=productRepository.findByCategories(category);
		}
		return productList;
		}catch(Exception e) {
			throw e;
		}
	}
	
	public String updateProduct(Product product)throws Exception {
		List<Category>listCategory = new ArrayList<>();
		try {
			if(null != product.getProductId()) {
				Product check = productRepository.findById(product.getProductId()).orElse(null);
				if(null != check) {
					check.setPrice(product.getPrice());
					check.setName(product.getName());
					if(null != product.getCategoryIds()) {
						for(Long id:product.getCategoryIds()) {
							Category c = categoryRepository.findById(id).orElse(null);
							if(null != c) {
								listCategory.add(c);
							}
						}
						check.setCategories(listCategory);
						productRepository.save(check);
					}
				}
			}
			
			return "product updated";
		}catch(Exception e ) {
			throw e;
		}
	}
}
