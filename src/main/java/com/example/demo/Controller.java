package com.example.demo;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {
	
	@Autowired
	public CategoryService categoryService;
	
	@Autowired
	public ProductService productService;
	
	@Autowired
	public ChildCategoryService childCategoryService;

	/**
	 * Add Category
	 * 
	 */
	@PostMapping("/add-category")
	public String addCategory(@RequestBody Category category ) {
		try {
			return categoryService.addCategory(category);
		}catch(Exception e) {
			e.printStackTrace();
			return "Category not saved";
		}
	}
	
	/**
	 * Add product and map with category
	 * 
	 */
	@PostMapping("/add-product")
	public String addProduct(@RequestBody Product product ) {
		try {
			return productService.addProduct(product);
		}catch(Exception e) {
			e.printStackTrace();
			return"product not saved";
		}
	}
	
	/**
	 * Get all categories with child categories
	 * 
	 */
	@GetMapping("/getall-categories")
	public List<Category> getCategories() {
		try {
			return categoryService.getAllCategories();
		}catch(Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * Get products mapped to category with categoryId
	 * 
	 */
	@GetMapping("/getproduct-bycategory/{Id}")
	public List<Product> getProductByCategory(@PathVariable Long Id) {
		try {
			return productService.getProductByCategory(Id);
		}catch(Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * update product
	 * 
	 */
	@PutMapping("/update-product")
	public String updateProduct(@RequestBody Product product ) {
		try {
			 return productService.updateProduct(product);
		}catch(Exception e) {
			e.printStackTrace();
			return "Product not updated";
		}
	}
	
	/**
	 * Add child category and mapp with category
	 * 
	 */
	@PostMapping("/add-childcategory")
	public String addChildCategory(@RequestBody ChildCategory childCategory ) {
		try {
			return childCategoryService.addChildCategory(childCategory);
		}catch(Exception e) {
			e.printStackTrace();
			return "Child Category not saved";
		}
	}

}
