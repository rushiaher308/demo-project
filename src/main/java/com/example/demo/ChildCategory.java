package com.example.demo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name="child_category")
public class ChildCategory implements Serializable{
	
	private static final long serialVersionUID = -6975302263393993426L;

	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	@Column (name = "childcategory_id", unique = true, nullable = false)
	private Long childCategoryId;
	
	@Column(name = "childcategory_name", length = 50)	
	private String childCategoryName;
	
	@JsonBackReference
	@ManyToOne(targetEntity = Category.class)
	@JoinColumn(name= "category_id")
	private Category category;
	
	@Transient
	private Long categoryId;
	
	@Transient
	List<SubChildCategory> subChildCategory = new ArrayList<SubChildCategory>();

	public Long getChildCategoryId() {
		return childCategoryId;
	}

	public void setChildCategoryId(Long childCategoryId) {
		this.childCategoryId = childCategoryId;
	}

	public String getChildCategoryName() {
		return childCategoryName;
	}

	public void setChildCategoryName(String childCategoryName) {
		this.childCategoryName = childCategoryName;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public Long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}

	public List<SubChildCategory> getSubChildCategory() {
		return subChildCategory;
	}

	public void setSubChildCategory(List<SubChildCategory> subChildCategory) {
		this.subChildCategory = subChildCategory;
	}
	
	
	
}
