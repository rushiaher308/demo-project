package com.example.demo;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepository extends JpaRepository<Product,Long>{
	
	public List<Product> findByCategories(Category category);
	
	public Product findByName(String Name);

}
