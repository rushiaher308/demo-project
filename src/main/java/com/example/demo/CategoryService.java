package com.example.demo;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CategoryService {
	
	@Autowired
	private CategoryRepository categoryRepository;
	
	@Autowired
	private ChildCategoryRepository childCategoryRepository;
	
	public String addCategory(Category category) throws Exception {
		try {
			if(null != categoryRepository.findByName(category.getName())) {
				return "Category Already present";

			}else {
				categoryRepository.save(category);
				return "Category saved";
			}
		}catch(Exception e) {
			throw e;
		}
	}
	
	@SuppressWarnings("unused")
	public List<Category> getAllCategories()throws Exception {
		List<Category>categoryList = new ArrayList<>();
		List<ChildCategory>childcategoryList = new ArrayList<>();
		try {
		categoryList = categoryRepository.findAll();
		for(Category c:categoryList) {
		childcategoryList = childCategoryRepository.findByCategory(c);
		if(null != childcategoryList) {
			c.setChildCatgories(childcategoryList);
		}
		}
		return categoryList;
		}catch(Exception e) {
			throw e;
		}
	}
		

}
