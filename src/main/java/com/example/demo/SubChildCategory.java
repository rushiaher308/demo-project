package com.example.demo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name="sub_child_category")
public class SubChildCategory implements Serializable{
	
	private static final long serialVersionUID = -6975302263393993426L;

	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	@Column (name = "subchildcategory_id", unique = true, nullable = false)
	private Long subchildCategoryId;
	
	@Column(name = "subchildcategory_name", length = 50)	
	private String subchildCategoryName;
	
	@JsonBackReference
	@ManyToOne(targetEntity = Category.class)
	@JoinColumn(name= "childcategory_id")
	private ChildCategory childCategory;
	
	@Transient
	private Long childCategoryId;

	public Long getSubchildCategoryId() {
		return subchildCategoryId;
	}

	public void setSubchildCategoryId(Long subchildCategoryId) {
		this.subchildCategoryId = subchildCategoryId;
	}

	public String getSubchildCategoryName() {
		return subchildCategoryName;
	}

	public void setSubchildCategoryName(String subchildCategoryName) {
		this.subchildCategoryName = subchildCategoryName;
	}

	public ChildCategory getChildCategory() {
		return childCategory;
	}

	public void setChildCategory(ChildCategory childCategory) {
		this.childCategory = childCategory;
	}

	public Long getChildCategoryId() {
		return childCategoryId;
	}

	public void setChildCategoryId(Long childCategoryId) {
		this.childCategoryId = childCategoryId;
	}

}
