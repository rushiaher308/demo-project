package com.example.demo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ChildCategoryRepository extends JpaRepository<ChildCategory,Long> {
	
	public List<ChildCategory> findByCategory(Category category);
	
	public ChildCategory findByChildCategoryName(String ChildCategoryName);

}
