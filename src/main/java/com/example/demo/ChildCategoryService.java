package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ChildCategoryService {
	
	@Autowired
	private CategoryRepository categoryRepository;
	
	@Autowired
	private ChildCategoryRepository childCategoryRepository;
	
	public String addChildCategory(ChildCategory childCategory) throws Exception {
		try {
			if(null!=childCategoryRepository.findByChildCategoryName(childCategory.getChildCategoryName())) {
				return "ChildCategory already present";
			}else {
			if(null != childCategory.getCategoryId()) {
				Category check = categoryRepository.findById(childCategory.getCategoryId()).orElse(null);
				if(null != check) {
					childCategory.setCategory(check);
				}
			}
			childCategoryRepository.save(childCategory);
			return "ChildCategory saved";
			}
		}catch(Exception e) {
			throw e;
		}
	}
}
