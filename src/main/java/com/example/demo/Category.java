package com.example.demo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name="category")
public class Category implements Serializable {
	
	
	private static final long serialVersionUID = -6975302262293893426L;

	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="category_id",unique=true,nullable=false)
	private Long categoryId;
	
	@Column(name="category_name")
	private String name;
	
	@JsonBackReference
	@ManyToMany(mappedBy="categories")
	private List<Product>products = new ArrayList<Product>();
	
	@Transient
	private List<ChildCategory>childCatgories = new ArrayList<>();

	public Long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Product> getProducts() {
		return products;
	}

	public void setProducts(List<Product> products) {
		this.products = products;
	}

	public List<ChildCategory> getChildCatgories() {
		return childCatgories;
	}

	public void setChildCatgories(List<ChildCategory> childCatgories) {
		this.childCatgories = childCatgories;
	}

}
